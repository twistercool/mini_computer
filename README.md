# Installation
   To install this library, go to the `./build` folder and run `sudo make`.

   To use the library, make sure that it's installed first, then include the header file by adding `#include <mini_computer.h>` to the appropriate location.

   When compiling, add the library flag with `-lmini_computer`, like this: `gcc program_that_uses_the_lib.c -lmini_computer -o output_bin`.
   This should work with any compiler.

# How to use it

   The intended way to use the library is by creating GATE\* structs with the `new_gate(gateTyoe input_type, ...)` function while specifying their gateType (and, nand, out\_gate, triple\_and, etc...) and their inputs.

   The initial gates of a circuit should have type `out_gate`, which means that they don't take as inputs other logic gates, they are the inputs to other logic gates.

   To compute the output of a gate, call the function `compute_output(GATE*)` on the output gates.
   This function will calculate the output of all gates recursively until the input gates, so you should only call it on the output gates.

   The output of a gate is a bool contained in the GATE struct that can be accessed with the arrow operator such as: `created_gate->output`.

   The gates created should then be freed after they're used, with `free_gate(GATE*)`.

   More information for all the gates types can be found in the header file in `./src/mini_computer.h`.

# Example code

   Here is example code for [this simple circuit](https://cloud.brassart.xyz/s/P832M7RQNQHdGJ3):

```c
#include <mini_computer.h>

int main(void) {
   // create and initialise all the gates, 'true' is the input by the user, it can be false
   GATE* gate_A = new_gate(out_gate, true);
   GATE* gate_B = new_gate(out_gate, true);
   GATE* gate_X = new_gate(and, gate_A, gate_B);
   GATE* gate_Y = new_gate(not, gate_B);
   GATE* gate_Q = new_gate(or, gate_X, gate_Y);

   // calculate the output of the output gate
   compute_output(gate_Q);

   printf("The output of the last gate is: %u\n", gate_Q->output);

   // free the gates after using them
   free_gate(gate_A);
   free_gate(gate_B);
   free_gate(gate_X);
   free_gate(gate_Y);
   free_gate(gate_Q);
}
```
