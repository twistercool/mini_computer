#ifndef __MINI_COMPUTER_HEADER_H_
#define __MINI_COMPUTER_HEADER_H_

#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

enum gateType {
   and,
   triple_and,
   quad_and,
   quint_and,
   not,
   xor,
   nand,
   triple_nand,
   quad_nand,
   quint_nand,
   nor,
   triple_nor,
   quad_nor,
   xnor,
   out_gate,
   or
};

typedef struct GATE GATE;
struct GATE {
   GATE *input_gate_0;
   GATE *input_gate_1;
   enum gateType type;
   bool output;
};

/*
   Expected inputs:
      1) The type of gate wanted (and gate, or gate, not gate, etc...) defined
         by a gateType
      2) Extra arguments depending on the gate:
         For out_gate: 1 extra argument, the output, a bool.
         For not: 1 extra argument, the input, 1 GATE*.
         For triple_and, triple_nand, triple_nor: 3 extra arguments, the inputs,
            3 GATE*.
         For quad_and, quad_nand, quad_nor: 4 extra arguments, 4 GATE*.
         For quint_and, quint_nand: 5 extra arguments, 5 GATE*.
         For all other logic gates, the inputs, 2 GATE*.

   triple_and works by creating two and gates, and letting the output of one
   feed into the second one. The created and gate will always be input_gate_0.
*/
GATE *new_gate(enum gateType input_type, ...);

void free_gate(GATE*);

bool compute_output(GATE*);

#endif
