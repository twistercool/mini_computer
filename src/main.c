#include "mini_computer.h"

/*
   Expected inputs:
      1) The type of gate wanted (and gate, or gate, not gate, etc...) defined
         by an enum gateType
      2) Extra arguments depending on the gate:
         For out_gate: 1 extra argument, the output, a bool.
         For not: 1 extra argument, the input, 1 GATE*.
         For triple_and, triple_nand, triple_nor: 3 extra arguments, the inputs,
            3 GATE*.
         For quad_and, quad_nand, quad_nor: 4 extra arguments, 4 GATE*.
         For quint_and, quint_nand: 5 extra arguments, 5 GATE*.
         For all other logic gates, the inputs, 2 GATE*.

   triple_and works by creating two and gates, and letting the output of one
   feed into the second one. The created and gate will always be input_gate_0.
*/
GATE *new_gate(enum gateType input_gateType, ...) {
   GATE *gate_ptr;
   if ( !(gate_ptr = malloc(sizeof(struct GATE))) ) {
      fprintf(stderr, "Failed to allocate memory for gate\n");
      exit(1);
   }

   gate_ptr->type = input_gateType;

   va_list ap;
   va_start(ap, input_gateType);

   switch (input_gateType) {
      case quint_and:
         gate_ptr->input_gate_0 = new_gate(triple_and, va_arg(ap, GATE*), va_arg(ap, GATE*), va_arg(ap, GATE*));
         gate_ptr->input_gate_1 = new_gate(and, va_arg(ap, GATE*), va_arg(ap, GATE*));
         break;
      case quint_nand:
         gate_ptr->input_gate_0 = new_gate(triple_nand, va_arg(ap, GATE*), va_arg(ap, GATE*), va_arg(ap, GATE*));
         gate_ptr->input_gate_1 = new_gate(nand, va_arg(ap, GATE*), va_arg(ap, GATE*));
         break;
      case quad_nand:
         gate_ptr->input_gate_0 = new_gate(nand, va_arg(ap, GATE*), va_arg(ap, GATE*));
         gate_ptr->input_gate_1 = new_gate(nand, va_arg(ap, GATE*), va_arg(ap, GATE*));
         break;
      case quad_and:
         gate_ptr->input_gate_0 = new_gate(and, va_arg(ap, GATE*), va_arg(ap, GATE*));
         gate_ptr->input_gate_1 = new_gate(and, va_arg(ap, GATE*), va_arg(ap, GATE*));
         break;
      case quad_nor:
         gate_ptr->input_gate_0 = new_gate(or, va_arg(ap, GATE*), va_arg(ap, GATE*));
         gate_ptr->input_gate_1 = new_gate(or, va_arg(ap, GATE*), va_arg(ap, GATE*));
         break;
      case triple_and:
         gate_ptr->input_gate_0 = new_gate(and, va_arg(ap, GATE*), va_arg(ap, GATE*));
         gate_ptr->input_gate_1 = va_arg(ap, GATE*);
         break;
      case triple_nand:
         gate_ptr->input_gate_0 = new_gate(nand, va_arg(ap, GATE*), va_arg(ap, GATE*));
         gate_ptr->input_gate_1 = va_arg(ap, GATE*);
         break;
      case triple_nor:
         gate_ptr->input_gate_0 = new_gate(or, va_arg(ap, GATE*), va_arg(ap, GATE*));
         gate_ptr->input_gate_1 = va_arg(ap, GATE*);
         break;
      case and:
      case or:
      case xor:
      case nor:
      case xnor:
      case nand:
         gate_ptr->input_gate_0 = va_arg(ap, GATE*);
         gate_ptr->input_gate_1 = va_arg(ap, GATE*);
         break;
      case not:
         gate_ptr->input_gate_0 = va_arg(ap, GATE*);
         break;
      case out_gate:
         /* va_args "upgrades" the type of the arguments to int */
         gate_ptr->output = (bool)va_arg(ap, int);
         break;
      default:
         fprintf(stderr, "Unknown gate type: %i\n", input_gateType);
         va_end(ap);
         exit(1);
         break;
   }

   va_end(ap);

   return gate_ptr;
}

void free_gate(GATE *input_gate) {
   switch (input_gate->type) {
      case quad_and:
      case quad_nand:
      case quad_nor:
      case quint_and:
      case quint_nand:
         free_gate(input_gate->input_gate_1);
         free_gate(input_gate->input_gate_0);
         free(input_gate);
         break;
      case triple_and:
      case triple_nand:
      case triple_nor:
         free_gate(input_gate->input_gate_0);
         free(input_gate);
         break;
      case and:
      case not:
      case xor:
      case nand:
      case nor:
      case xnor:
      case out_gate:
      case or:
         free(input_gate);
         break;
      default:
         fprintf(stderr, "Unknown gate type: %i\n", input_gate->type);
         exit(1);
         break;
   }
}

bool compute_output(GATE *in) {
   switch (in->type) {
      case triple_and:
      case quad_and:
      case quint_and:
      case and:
         in->output = compute_output(in->input_gate_0) & compute_output(in->input_gate_1);
         break;
      case or:
         in->output = compute_output(in->input_gate_0) | compute_output(in->input_gate_1);
         break;
      case xor:
         in->output = compute_output(in->input_gate_0) ^ compute_output(in->input_gate_1);
         break;
      case not:
         in->output = !compute_output(in->input_gate_0);
         break;
      case nand:
      case triple_nand:
      case quad_nand:
      case quint_nand:
         in->output = !(compute_output(in->input_gate_0) & compute_output(in->input_gate_1));
         break;
      case nor:
      case triple_nor:
      case quad_nor:
         in->output = !(compute_output(in->input_gate_0) | compute_output(in->input_gate_1));
         break;
      case xnor:
         in->output = !(compute_output(in->input_gate_0) ^ compute_output(in->input_gate_1));
         break;
      case out_gate:
         /* output assumed to be set */
         break;
      default:
         fprintf(stderr, "Unknown gate type: %b\n", in->type);
         exit(1);
         break;
   }

   return in->output;
}
